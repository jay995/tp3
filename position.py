__author__ = 'Jeremie Hamel et Vincent Gariepy'


class Position:
    """Une position à deux coordonnées: ligne et colonne. La convention utilisée est celle de la notation matricielle :
    le coin supérieur gauche d'une matrice est dénoté (0, 0) (ligne 0 et colonne 0). On additionne une unité de colonne
    lorsqu'on se déplace vers la droite, et une unité de ligne lorsqu'on se déplace vers le bas.

    +-------+-------+-------+-------+
    | (0,0) | (0,1) | (0,2) |  ...  |
    | (1,0) | (1,1) | (1,2) |  ...  |
    | (2,0) | (2,1) | (2,2) |  ...  |
    |  ...  |  ...  |  ---  |  ...  |
    +-------+-------+-------+-------+

    Attributes:
        ligne (int): La ligne associée à la position.
        colonne (int): La colonne associée à la position

    """
    def __init__(self, ligne, colonne):
        """Constructeur de la classe Position. Initialise les deux attributs de la classe.

        Args:
            ligne (int): La ligne à considérer dans l'instance de Position.
            colonne (int): La colonne à considérer dans l'instance de Position.

        """
        self.ligne = int(ligne)
        self.colonne = int(colonne)

    def positions_diagonales_bas(self):
        """Retourne une liste contenant les deux positions diagonales bas à partir de la position actuelle.

        Note:
            Dans cette méthode et les prochaines, vous n'avez pas à valider qu'une position est "valide", car dans le
            contexte de cette classe toutes les positions (même négatives) sont permises.

        Returns:
            list: La liste des deux positions.

        """

        a = self.ligne + 1
        b = self.colonne - 1
        c = self.colonne + 1
        x = Position(a,b)
        y = Position(a,c)
        list = [x, y]

        return list


    def positions_diagonales_haut(self):
        """Retourne une liste contenant les deux positions diagonales haut à partir de la position actuelle.

        Returns:
            list: La liste des deux positions.

        """

        a = self.ligne - 1
        b = self.colonne - 1
        c = self.colonne + 1
        x = Position(a,b)
        y = Position(a,c)
        list = [x, y]

        return list


    def quatre_positions_diagonales(self):
        """Retourne une liste contenant les quatre positions diagonales à partir de la position actuelle.

        Returns:
            list: La liste des quatre positions.

        """

        x1 = self.ligne + 1
        x2 = self.ligne - 1
        y1 = self.colonne + 1
        y2 = self.colonne - 1
        a = Position(x1,y1)
        b = Position(x1,y2)
        c = Position(x2,y1)
        d = Position(x2,y2)
        list = [a, b, c, d]

        return list


    def quatre_positions_sauts(self):
        """Retourne une liste contenant les quatre "sauts" diagonaux à partir de la position actuelle. Les positions
        retournées sont donc en diagonale avec la position actuelle, mais a une distance de 2.

        Returns:
            list: La liste des quatre positions.

        """
        x1 = self.ligne + 2
        x2 = self.ligne - 2
        y1 = self.colonne + 2
        y2 = self.colonne - 2
        a = Position(x1,y1)
        b = Position(x1,y2)
        c = Position(x2,y1)
        d = Position(x2,y2)
        list = [a, b, c, d]

        return list

    def __eq__(self, other):
        """Méthode spéciale indiquant à Python comment vérifier si deux positions sont égales. On compare simplement
        la ligne et la colonne de l'objet actuel et de l'autre objet.

        """
        return self.ligne == other.ligne and self.colonne == other.colonne

    def __repr__(self):
        """Méthode spéciale indiquant à Python comment représenter une instance de Position par une chaîne de
        caractères. Notamment utilisé pour imprimer une position à l'écran.

        """
        return '({}, {})'.format(self.ligne, self.colonne)

    def __hash__(self):
        """Méthode spéciale indiquant à Python comment "hasher" une Position. Cette méthode est nécessaire si on veut
        utiliser une classe que nous avons définie nous mêmes comme clé d'un dictionnaire.

        """
        return hash(str(self))

if __name__ == '__main__':
    piece1 = Position(7, 0)
    piece2 = Position(3, 3)
    piece3 = Position(6, 7)
    piece4 = Position(1, 0)

    assert piece1.positions_diagonales_bas() == [Position(8, -1), Position(8, 1)], "Erreur dans les diagonales-bas"
    assert piece2.positions_diagonales_bas() == [Position(4, 2), Position(4, 4)], "Erreur dans les diagonales-bas"
    assert piece3.positions_diagonales_bas() == [Position(7, 6), Position(7, 8)], "Erreur dans les diagonales-bas"
    assert piece4.positions_diagonales_bas() == [Position(2, -1), Position(2, 1)], "Erreur dans les diagonales-bas"

    assert piece1.positions_diagonales_haut() == [Position(6, -1), Position(6, 1)], "Erreur dans les diagonales-haut"
    assert piece2.positions_diagonales_haut() == [Position(2, 2), Position(2, 4)], "Erreur dans les diagonales-haut"
    assert piece3.positions_diagonales_haut() == [Position(5, 6), Position(5, 8)], "Erreur dans les diagonales-haut"
    assert piece4.positions_diagonales_haut() == [Position(0, -1), Position(0, 1)], "Erreur dans les diagonales-haut"

    #assert piece1.quatre_positions_diagonales() == [Position(8, 1), Position(8, -1), Position(6, 1), Position(6, -1)], "Erreur dans les positions quatres diagonales"
    #assert piece2.quatre_positions_diagonales() == [Position(4, 2), Position(4, 4), Position(2, 2), Position(2, 4)], "Erreur dans les positions quatres diagonales"
    #assert piece3.quatre_positions_diagonales() == [Position(7, 6), Position(7, 8), Position(5, 6), Position(5, 8)], "Erreur dans les positions quatres diagonales"
    #assert piece4.quatre_positions_diagonales() == [Position(2, -1), Position(2, 1), Position(0, -1), Position(0, 1)], "Erreur dans les positions quatres diagonales"

    #assert piece1.quatre_positions_sauts() == [Position(9, 2), Position(9, -2), Position(5, 2), Position(5, -2)], "Erreur dans les quatres positions avec saut"
    #assert piece2.quatre_positions_sauts() == [Position(5, 5), Position(5, 1), Position(4, 9), Position(4, 5)], "Erreur dans les quatres positions avec saut"
    #assert piece4.quatre_positions_sauts() == [Position(3, 2), Position(3, -2), Position(-1, 2), Position(-1, -2)], "Erreur dans les quatres positions avec saut"

    print("Tout les tests sont réussis")

    #yolo