__author__ = 'Jeremie Hamel et Vincent Gariepy'

from piece import Piece
from position import Position


class Damier:
    """Plateau de jeu d'un jeu de dames. Contient un ensemble de pièces positionnées à une certaine position
    sur le plateau.

    Attributes:
        cases (dict): Dictionnaire dont une clé représente une Position, et une valeur correspond à la Piece
            positionnée à cet endroit sur le plateau.

        n_lignes (int): Le nombre de lignes du plateau. La valeur est 8 (constante).
        n_colonnes (int): Le nombre de colonnes du plateau. La valeur est 8 (constante).

    """

    def __init__(self):
        """Constructeur du Damier. Initialise un damier initial de 8 lignes par 8 colonnes.

        """
        self.n_lignes = 8
        self.n_colonnes = 8

        self.cases = {
            Position(7, 0): Piece("blanc", "pion"),
            Position(7, 2): Piece("blanc", "pion"),
            Position(7, 4): Piece("blanc", "pion"),
            Position(7, 6): Piece("blanc", "pion"),
            Position(6, 1): Piece("blanc", "pion"),
            Position(6, 3): Piece("blanc", "pion"),
            Position(6, 5): Piece("blanc", "pion"),
            Position(6, 7): Piece("blanc", "pion"),
            Position(5, 0): Piece("blanc", "pion"),
            Position(5, 2): Piece("blanc", "pion"),
            Position(5, 4): Piece("blanc", "pion"),
            Position(5, 6): Piece("blanc", "pion"),
            Position(2, 1): Piece("noir", "pion"),
            Position(2, 3): Piece("noir", "pion"),
            Position(2, 5): Piece("noir", "pion"),
            Position(2, 7): Piece("noir", "pion"),
            Position(1, 0): Piece("noir", "pion"),
            Position(1, 2): Piece("noir", "pion"),
            Position(1, 4): Piece("noir", "pion"),
            Position(1, 6): Piece("noir", "pion"),
            Position(0, 1): Piece("noir", "pion"),
            Position(0, 3): Piece("noir", "pion"),
            Position(0, 5): Piece("noir", "pion"),
            Position(0, 7): Piece("noir", "pion"),
        }

    def recuperer_piece_a_position(self, position):
        """Récupère une pièce dans le damier à partir d'une position.

        Args:
            position (Position): La position où récupérer la pièce.

        Returns:
            La pièce (de type Piece) à la position reçue en argument, ou None si aucune pièce n'était à cette position.

        """
        if position in self.cases.keys():
            return self.cases[position]
        else:
            return None


    def position_est_dans_damier(self, position):
        """Vérifie si les coordonnées d'une position sont dans les bornes du damier (entre 0 inclusivement et le nombre
        de lignes/colonnes, exclusement.

        Args:
            position (Position): La position à valider.

        Returns:
            bool: True si la position est dans les bornes, False autrement.

        """
        ligne = position.ligne
        colonne = position.colonne

        if ligne < self.n_lignes and ligne >= 0 and colonne < self.n_colonnes and colonne >= 0:
            return True

        else:
            return False

    def piece_peut_se_deplacer_vers(self, position_piece, position_cible):
        """Cette méthode détermine si une pièce (à la position reçue) peut se déplacer à une certaine position cible.
        On parle ici d'un déplacement standard (et non une prise).

        Une pièce doit être positionnée à la position_piece reçue en argument (retourner False autrement).

        Une pièce de type pion ne peut qu'avancer en diagonale (vers le haut pour une pièce blanche, vers le bas pour
        une pièce noire). Une pièce de type dame peut avancer sur n'importe quelle diagonale, peu importe sa couleur.
        Une pièce ne peut pas se déplacer sur une case déjà occupée par une autre pièce. Une pièce ne peut pas se
        déplacer à l'extérieur du damier.

        Args:
            position_piece (Position): La position de la pièce source du déplacement.
            position_cible (Position): La position cible du déplacement.

        Returns:
            bool: True si la pièce peut se déplacer à la position cible, False autrement.
po
        """

        if self.position_est_dans_damier(position_cible) == False:
            return False

        elif str(self.recuperer_piece_a_position(position_piece)) == "x":
            if position_cible in position_piece.positions_diagonales_bas():
                if self.recuperer_piece_a_position(position_cible) is not None:
                    return False
                elif self.position_est_dans_damier(position_cible) is None:
                    return False
                else:
                    return True
            else:
                return False

        elif str(self.recuperer_piece_a_position(position_piece)) == "o":
            if position_cible in position_piece.positions_diagonales_haut():
                if self.recuperer_piece_a_position(position_cible) is not None:
                    return False
                elif self.position_est_dans_damier(position_cible) is None:
                    return False
                else:
                    return True
            else:
                return False


        elif str(self.recuperer_piece_a_position(position_piece)) == "X" or str(self.recuperer_piece_a_position(position_piece) == "O"):

            if position_cible in position_piece.quatre_positions_diagonales():
                if self.recuperer_piece_a_position(position_cible) is not None:
                    return False
                elif self.position_est_dans_damier(position_cible) is None:
                    return False
                else:
                    return True
            else:
                return False

    def piece_peut_sauter_vers(self, position_piece, position_cible):
        """Cette méthode détermine si une pièce (à la position reçue) peut sauter vers une certaine position cible.
        On parle ici d'un déplacement qui "mange" une pièce adverse.

        Une pièce doit être positionnée à la position_piece reçue en argument (retourner False autrement).

        Une pièce ne peut que sauter de deux cases en diagonale. N'importe quel type de pièce (pion ou dame) peut sauter
        vers l'avant ou vers l'arrière. Une pièce ne peut pas sauter vers une case qui est déjà occupée par une autre
        pièce. Une pièce ne peut faire un saut que si elle saute par dessus une pièce de couleur adverse.

        Args:
            position_piece (Position): La position de la pièce source du saut.
            position_cible (Position): La position cible du saut.

        Returns:
            bool: True si la pièce peut sauter vers la position cible, False autrement.

        """
        x = 0

        if self.position_est_dans_damier(position_cible) == False:
            return False

        elif str(self.recuperer_piece_a_position(position_piece)) == "o" or str(self.recuperer_piece_a_position(position_piece)) == "O":
            if position_cible in position_piece.quatre_positions_sauts() and self.recuperer_piece_a_position(position_cible) is None:
                for prise_potentielle in position_piece.quatre_positions_diagonales():
                    if prise_potentielle in position_cible.quatre_positions_diagonales() and str(self.recuperer_piece_a_position(prise_potentielle)) == "x" or self.recuperer_piece_a_position(prise_potentielle) == "X":     #La pièce à capturer est obligatoirement sur le chemin de la case cible
                        return True


            else:
                return False

        elif str(self.recuperer_piece_a_position(position_piece)) == "x" or str(self.recuperer_piece_a_position(position_piece)) == "X":
            if position_cible in position_piece.quatre_positions_sauts() and self.recuperer_piece_a_position(position_cible) is None:
                for prise_potentielle in position_piece.quatre_positions_diagonales():
                    if prise_potentielle in position_cible.quatre_positions_diagonales() and str(self.recuperer_piece_a_position(prise_potentielle)) == "o" or str(self.recuperer_piece_a_position(prise_potentielle)) == "O":     #La pièce à capturer est obligatoirement sur le chemin de la case cible
                        return True

            else:
                return False



    def piece_peut_se_deplacer(self, position_piece):
        """Vérifie si une pièce à une certaine position a la possibilité de se déplacer (sans faire de saut).

        ATTENTION: N'oubliez pas qu'étant donné une position, il existe une méthode dans la classe Position retournant
        les positions des quatre déplacements possibles.

        Args:
            position_piece (Position): La position source.

        Returns:
            bool: True si une pièce est à la position reçue et celle-ci peut se déplacer, False autrement.

        """
        x = 0       #compteur de déplacements possibles

        if self.recuperer_piece_a_position(position_piece) == None:
            print ("Il n'y a pas de pièce à cette position.")
            return False

        if str(self.recuperer_piece_a_position(position_piece)) == "x":        #pour les pions noirs
            for mouvement_potentiel in position_piece.positions_diagonales_bas():
                if self.piece_peut_se_deplacer_vers(position_piece, mouvement_potentiel) == True:
                    x += 1
                else:
                    x = x

        if str(self.recuperer_piece_a_position(position_piece)) == "o":        #pour les pions blancs
            for mouvement_potentiel in position_piece.positions_diagonales_haut():
                if self.piece_peut_se_deplacer_vers(position_piece, mouvement_potentiel) == True:
                    x += 1
                else:
                    x = x

        if str(self.recuperer_piece_a_position(position_piece)) == "X" or str(self.recuperer_piece_a_position(position_piece) == "O"):        #pour les dames
            for mouvement_potentiel in position_piece.quatre_positions_diagonales():
                if self.piece_peut_se_deplacer_vers(position_piece, mouvement_potentiel) == True:
                    x += 1
                else:
                    x = x

        if x == 0:
            return False
        else:
            return True


    def piece_peut_faire_une_prise(self, position_piece):
        """Vérifie si une pièce à une certaine position a la possibilité de faire une prise.

        ATTENTION: N'oubliez pas qu'étant donné une position, il existe une méthode dans la classe Position retournant
        les positions des quatre sauts possibles.

        Args:
            position_piece (Position): La position source.

        Returns:
            bool: True si une pièce est à la position reçue et celle-ci peut faire une prise. False autrement.

        """

        x = 0
        for position in position_piece.quatre_positions_sauts():
            if self.piece_peut_sauter_vers(position_piece, position) == True:
                x = 1

        if x == 1:
            return True
        else:
            return False




    def piece_de_couleur_peut_se_deplacer(self, couleur):
        """Vérifie si n'importe quelle pièce d'une certaine couleur reçue en argument a la possibilité de se déplacer
        vers une case adjacente (sans saut).

        ATTENTION: Réutilisez les méthodes déjà programmées!

        Args:
            couleur (str): La couleur à vérifier.

        Returns:
            bool: True si une pièce de la couleur reçue peut faire un déplacement standard, False autrement.
        """

        if couleur == "noir":
            for piece in self.cases.keys():
                if str(self.recuperer_piece_a_position(piece)) == "x" or str(self.recuperer_piece_a_position(piece)) == "X":
                    if self.piece_peut_se_deplacer(piece) == True:
                        return True
            return False

        if couleur == "blanc":
            for piece in self.cases.keys():
                if str(self.recuperer_piece_a_position(piece)) == "o" or str(self.recuperer_piece_a_position(piece)) == "O":
                    if self.piece_peut_se_deplacer(piece) == True:
                        return True
            return False

    def piece_de_couleur_peut_faire_une_prise(self, couleur):
        """Vérifie si n'importe quelle pièce d'une certaine couleur reçue en argument a la possibilité de faire un
        saut, c'est à dire vérifie s'il existe une pièce d'une certaine couleur qui a la possibilité de prendre une
        pièce adverse.

        ATTENTION: Réutilisez les méthodes déjà programmées!

        Args:
            couleur (str): La couleur à vérifier.

        Returns:
            bool: True si une pièce de la couleur reçue peut faire un saut (une prise), False autrement.
        """

        if couleur == "noir":
            for piece in self.cases.keys():
                if str(self.recuperer_piece_a_position(piece)) == "x" or str(self.recuperer_piece_a_position(piece)) == "X":
                    if self.piece_peut_faire_une_prise(piece) == True:
                        return True

        if couleur == "blanc":
            for piece in self.cases.keys():
                if str(self.recuperer_piece_a_position(piece)) == "o" or str(self.recuperer_piece_a_position(piece)) == "O":
                    if self.piece_peut_faire_une_prise(piece) == True:
                        return True




    def deplacer(self, position_source, position_cible):
        """Effectue le déplacement sur le damier. Si le déplacement est valide, on doit mettre à jour le dictionnaire
        self.cases, en déplaçant la pièce à sa nouvelle position (et possiblement en supprimant une adverse pièce qui a
        été prise).

        Cette méthode doit également:
        - Promouvoir un pion en dame si celui-ci atteint l'autre extrémité du plateau.
        - Retourner un message indiquant "ok", "prise" ou "erreur".

        ATTENTION: Si le déplacement est effectué, cette méthode doit retourner "ok" si aucune prise n'a été faite,
            et "prise" si une pièce a été prise.
        ATTENTION: Ne dupliquez pas de code! Vous avez déjà programmé (ou allez programmer) des méthodes permettant
            de valider si une pièce peut se déplacer vers un certain endroit ou non.

        Args:
            position_source (Position): La position source du déplacement.
            position_cible (Position): La position cible du déplacement.

        Returns:
            str: "ok" si le déplacement a été effectué sans prise, "prise" si une pièce adverse a été prise, et
                "erreur" autrement.

        """


        if position_cible in position_source.quatre_positions_diagonales():    #le joueur veut faire un déplacement
            dict = {Position(position_cible.ligne, position_cible.colonne): self.cases[position_source]}
            self.cases.update(dict)
            del self.cases[position_source]



            if str(self.recuperer_piece_a_position(position_source)) == "x":       #promouvoir une pièce noire
                if position_cible.ligne == 0:
                    Piece.promouvoir(damier.cases[position_cible])

            if str(self.recuperer_piece_a_position(position_source)) == "o":       #promouvoir une pièce blanche
                if position_cible.ligne == 7:
                    Piece.promouvoir(damier.cases[position_cible])

            return "ok"

        elif position_cible in position_source.quatre_positions_sauts():     #le joueur veut faire une prise
            for position in position_source.quatre_positions_diagonales():
                if position in position_cible.quatre_positions_diagonales():    #une seule position possible
                    del self.cases[position]      #position de la pièce mangée
            self.cases[position_cible] = self.cases[position_source]
            del self.cases[position_source]

            if str(self.recuperer_piece_a_position(position_cible)) == "o":       #promouvoir une pièce blanche
                if position_cible.ligne == 0:
                    Piece.promouvoir(damier.cases[position_cible])


            if str(self.recuperer_piece_a_position(position_source)) == "x":       #promouvoir une pièce noire
                if position_cible.ligne == 7:
                    Piece.promouvoir(damier.cases[position_cible])

            return "prise"

        else:
            return "Erreur: Déplacement invalide."


    def convertir_en_chaine(self):
        """Retourne une chaîne de caractères où chaque case est écrite sur une ligne distincte.
        Chaque ligne contient l'information suivante (respectez l'ordre et la manière de séparer les éléments):
        ligne,colonne,couleur,type

        Par exemple, un damier à deux pièces (un pion noir en (1, 2) et une dame blanche en (6, 1)) serait représenté
        par la chaîne suivante:

        1,2,noir,pion
        6,1,blanc,dame

        Cette méthode pourrait par la suite être réutilisée pour sauvegarder un damier dans un fichier.

        Returns:
            (str): La chaîne de caractères construite.

        """

        string1 = ""
        for piece in self.cases.keys():
            x = piece.ligne
            y = piece.colonne
            piece2 = self.cases[piece]
            c = piece2.couleur
            t = piece2.type_de_piece
            string = str(x) + ',' + str(y) + ',' + str(c)+ ',' + str(t)
            string1 = string1 + string + "\n"
        return(string1)

    def charger_dune_chaine(self, chaine):
        """Vide le damier actuel et le remplit avec les informations reçues dans une chaîne de caractères. Le format de
        la chaîne est expliqué dans la documentation de la méthode convertir_en_chaine.

        Args:
            chaine (str): La chaîne de caractères.

        """
        liste = chaine.splitlines()
        for piece in liste:
            x, y = piece[0],piece[2]
            if piece[4] == "n":
                couleur = "noir"
                if piece[9] == "p":
                    type_piece = "pion"
                else:
                    type_piece = "dame"
            else:
                couleur = "blanc"
                if piece[10] == "p":
                    type_piece = "pion"
                else:
                    type_piece = "dame"
            ma_piece = Position(x,y)
            t ={}
            t[ma_piece] = Piece(couleur, type_piece)

    def __repr__(self):
        """Cette méthode spéciale permet de modifier le comportement d'une instance de la classe Damier pour
        l'affichage. Faire un print(un_damier) affichera le damier à l'écran.

        """
        s = " +-0-+-1-+-2-+-3-+-4-+-5-+-6-+-7-+\n"
        for i in range(0, 8):
            s += str(i)+"| "
            for j in range(0, 8):
                if Position(i, j) in self.cases:
                    s += str(self.cases[Position(i, j)])+" | "
                else:
                    s += "  | "
            s += "\n +---+---+---+---+---+---+---+---+\n"

        return s


if __name__ == "__main__":
    # Ceci n'est pas le point d'entrée du programme principal, mais il vous permettra de faire de petits tests avec
    # vos fonctions du damier.
    damier = Damier()
    #print(damier)

    piece1 = Position(2, 3)

    position_cible = Position(3, 2)
    piece = Position(5, 2)
    position_cible2 = Position(4, 1)
    damier.deplacer(piece,Position(4,1))
    damier.deplacer(piece1,position_cible)
    damier.deplacer(position_cible2, Position(2, 3))
    damier.deplacer(Position(2,5), Position(3,4))
    damier.deplacer(Position(1,6), Position(2,5))
    damier.deplacer(Position(0,5), Position(1,6))
    damier.deplacer(Position(2,3), Position(0,5))
    damier.deplacer(Position(5,4), Position(4,5))

    print(damier.piece_peut_faire_une_prise(Position(3,4)))



    #damier.deplacer(Position(1, 4), Position(3, 2))


    print(damier)
    #print(damier.piece_de_couleur_peut_faire_une_prise("blanc"))
    #print(damier.piece_peut_faire_une_prise(Position(0, 5)))
    #print(damier.piece_peut_sauter_vers(Position(5,0), Position(3, 2)))

    #chaine = "fdgfdagfad\ndsgffags\n  sdfsd \n dsgfd"
    #damier.charger_dune_chaine(chaine)

