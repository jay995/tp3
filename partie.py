__author__ = 'Jeremie Hamel et Vincent Gariepy'

from damier import Damier
from position import Position


class Partie:
    """Gestionnaire de partie de dames.

    Attributes:
        damier (Damier): Le damier de la partie, contenant notamment les pièces.
        couleur_joueur_courant (str): Le joueur à qui c'est le tour de jouer.
        doit_prendre (bool): Un booléen représentant si le joueur actif doit absoluement effectuer une prise
            de pièce. Sera utile pour valider les mouvements et pour gérer les prises multiples.
        position_source_selectionnee (Position): La position source qui a été sélectionnée. Utile pour sauvegarder
            cette information avant de poursuivre. Contient None si aucune pièce n'est sélectionnée.
        position_source_forcee (Position): Une position avec laquelle le joueur actif doit absoluement jouer. Le
            seul moment où cette position est utilisée est après une prise: si le joueur peut encore prendre
            d'autres pièces adverses, il doit absolument le faire. Ce membre contient None si aucune position n'est
            forcée.

    """
    def __init__(self):
        """Constructeur de la classe Partie. Initialise les attributs à leur valeur par défaut. Le damier est construit
        avec les pièces à leur valeur initiales, le joueur actif est le joueur blanc, et celui-ci n'est pas forcé
        de prendre une pièce adverse. Aucune position source n'est sélectionnée, et aucune position source n'est forcée.

        """
        self.damier = Damier()
        self.couleur_joueur_courant = "blanc"
        self.doit_prendre = False
        self.position_source_selectionnee = None
        self.position_source_forcee = None

    def position_source_valide(self, position_source):
        """Vérifie la validité de la position source, notamment:
            - Est-ce que la position contient une pièce?
            - Est-ce que cette pièce est de la couleur du joueur actif?
            - Si le joueur doit absoluement continuer son mouvement avec une prise supplémentaire, a-t-il choisi la
              bonne pièce?

        Cette méthode retourne deux valeurs. La première valeur est Booléenne (True ou False), et la seconde valeur est
        un message d'erreur indiquant la raison pourquoi la position n'est pas valide (ou une chaîne vide s'il n'y a pas
        d'erreur).

        ATTENTION: Utilisez les attributs de la classe pour connaître les informations sur le jeu! (le damier, le joueur
            actif, si une position source est forcée, etc.

        ATTENTION: Vous avez accès ici à un attribut de type Damier. vous avez accès à plusieurs méthodes pratiques
            dans le damier qui vous simplifieront la tâche ici :)

        Args:
            position_source (Position): La position source à valider.

        Returns:
            bool, str: Un couple où le premier élément représente la validité de la position (True ou False), et le
                 deuxième élément est un message d'erreur (ou une chaîne vide s'il n'y a pas d'erreur).

        """


        if damier.piece_de_couleur_peut_faire_une_prise(self.couleur_joueur_courant)== True:
            if damier.piece_peut_faire_une_prise(position_source) == False:
                return "Une prise est possible.\nVeuillez choisir une piece pouvant faire une prise."
            else:
                x = 0


        piece = damier.recuperer_piece_a_position(position_source)

        if damier.position_est_dans_damier(position_source) == True:
            if damier.recuperer_piece_a_position(position_source) is not None:
                if piece.couleur == self.couleur_joueur_courant:
                    return True
                else:
                    return "Cette piece n'est pas de votre couleur.\nVeuillez entrer une position valide."

            else:
                return "Il n'y a pas de pièce à la position entrée.\nVeuillez entrer une position valide."
        else:
            return "La position entrée n'est pas dans le damier.\nVeuillez entrer une position valide."

    def position_cible_valide(self, position_cible):
        """Vérifie si la position cible est valide (en fonction de la position source sélectionnée). Doit non seulement
        vérifier si le déplacement serait valide (utilisez les méthodes que vous avez programmées dans le Damier!), mais
        également si le joueur a respecté la contrainte de prise obligatoire.

        Returns:
            bool, str: Deux valeurs, la première étant Booléenne et indiquant si la position cible est valide, et la
                seconde valeur est une chaîne de caractères indiquant un message d'erreur (ou une chaîne vide s'il n'y
                a pas d'erreur).

        """
        if damier.piece_de_couleur_peut_faire_une_prise(self.couleur_joueur_courant) == True:
            if position_cible not in self.position_source_selectionnee.quatre_positions_sauts():          #À fixer
                return "Une prise est possible.\nVeuillez faire effectuer prise."
        if damier.position_est_dans_damier(position_cible) == True:
            if str(damier.recuperer_piece_a_position(position_cible)) is not None:
                if damier.piece_peut_se_deplacer_vers(self.position_source_selectionnee, position_cible) == True or damier.piece_peut_sauter_vers(self.position_source_selectionnee, position_cible):
                    return True
                else:
                    return "Déplacement invalide.\nVeuillez entrer une autre position."
            else:
                return "La case cible est déjà occupée.\nVeuillez entrer une autre position."
        else:
            return "La position cible n'est pas dans le damier\nVeuillez entrer une autre position."



    def demander_positions_deplacement(self):
        """Demande à l'utilisateur les positions sources et cible, et valide ces positions. Cette méthode doit demander
        les positions à l'utilisateur tant que celles-ci sont invalides.

        Cette méthode ne doit jamais planter, peu importe ce que l'utilisateur entre. Voir le module 4.5 pour des
        trucs pour vérifier si une information fournie est un entier ou non.

        Returns:
            Position, Position: Un couple de deux positions (source et cible).

        """
        a = 0
        b = 0
        position_cible = Position(0, 0)
        print("C'est au tour du joueur", self.couleur_joueur_courant)
        while a == 0:
            x1 = (input("Sur quelle ligne est située la pièce à déplacer? "))
            y1 = input("Sur quelle colonne? ")
            self.position_source_selectionnee = Position(x1, y1)
            if self.position_source_valide(self.position_source_selectionnee) == True:
                a = 1
            else:
                print(self.position_source_valide(self.position_source_selectionnee))

        while b == 0:
            x2 = (input("Sur quelle ligne est située la case cible? "))
            y2 = input("Sur quelle colonne? ")
            position_cible = Position(x2, y2)
            if self.position_cible_valide(position_cible) == True:
                b = 1
            else:
                print(self.position_cible_valide(position_cible))

        return self.position_source_selectionnee, position_cible


    def tour(self):
        """Cette méthode effectue le tour d'un joueur, et doit effectuer les actions suivantes:
        - Assigne self.doit_prendre à True si le joueur courant a la possibilité de prendre une pièce adverse.  DONE
          (n'oubliez pas que vous avez accès à des méthodes dans la classe Position et la classe Damier!)
        - Demander les positions source et cible (utilisez self.demander_positions_deplacement!)    DONE
        - Effectuer le déplacement (à l'aide de la méthode du damier appropriée)    DONE
        - Si une pièce a été prise lors du déplacement, c'est encore au tour du même joueur si celui-ci peut encore DONE
          prendre une pièce adverse en continuant son mouvement. Utilisez les membres self.doit_prendre et
          self.position_source_forcee pour forcer ce prochain tour!
        - Si aucune pièce n'a été prise ou qu'aucun coup supplémentaire peut être fait avec la même pièce, c'est le
          tour du joueur adverse. Mettez à jour les attributs de la classe en conséquence.

        """
        deuxieme_deplacement_valide = False
        if damier.piece_de_couleur_peut_faire_une_prise(self.couleur_joueur_courant) == True:
            self.doit_prendre = True
        else:
            self.doit_prendre = False

        self.position_source_selectionnee, position_cible = self.demander_positions_deplacement()
        damier.deplacer(self.position_source_selectionnee, position_cible)
        if position_cible in self.position_source_selectionnee.quatre_positions_sauts():      #le déplacement a été un saut, donc une pièce a été prise
            while damier.piece_peut_faire_une_prise(position_cible) == True:
                self.doit_prendre = True
                self.position_source_forcee = position_cible
                print("La pièce déplacée peut faire une prise à nouveau.\nVeuillez effectuer une deuxième prise.")
                while deuxieme_deplacement_valide == False:
                    x = input("Sur quelle ligne est la case finale du deuxième déplacement? ")
                    y = input("Sur quelle colonne? ")
                    position_cible = Position(self.position_source_forcee, position_cible)
                    if self.position_cible_valide(position_cible) == True:
                        deuxieme_deplacement_valide = True
                        damier.deplacer(self.position_source_selectionnee, position_cible)
                    else:
                        print(self.position_cible_valide(position_cible))

        if self.couleur_joueur_courant == "blanc":      #Changement de joueur
            self.couleur_joueur_courant = "noir"

        elif self.couleur_joueur_courant == "noir":
            self.couleur_joueur_courant = "blanc"

        self.position_source_selectionnee = None        #Rétablissement des objets
        self.position_source_forcee = None
        self.doit_prendre = False


    def jouer(self):
        """Démarre une partie. Tant que le joueur courant a des déplacements possibles (utilisez les méthodes
        appriopriées!), un nouveau tour est joué.

        Returns:
            str: La couleur du joueur gagnant.
        """
        while damier.piece_de_couleur_peut_se_deplacer(self.couleur_joueur_courant) == True or damier.piece_de_couleur_peut_faire_une_prise(self.couleur_joueur_courant) == True:
            self.tour()
            print(damier)

        if self.couleur_joueur_courant == "blanc":
            return "noir"
        if self.couleur_joueur_courant == "noir":
            return "blanc"

    def sauvegarder(self, nom_fichier):
        """Sauvegarde une partie dans un fichier. Le fichier contiendra:
        - Une ligne indiquant la couleur du joueur courant.
        - Une ligne contenant True ou False, si le joueur courant doit absolument effectuer une prise à son tour.
        - Une ligne contenant None si self.position_source_forcee est à None, et la position ligne,colonne autrement.
        - Le reste des lignes correspondent au damier. Voir la méthode convertir_en_chaine du damier pour le format.

        ATTENTION: Lorsqu'on écrit ou lit dans un fichier texte, il faut s'assurer de bien convertir les variables
        dans le bon type.

        Exemple de contenu de fichier :

        blanc
        True
        6,1
        1,2,noir,dame
        1,6,blanc,pion
        4,1,noir,dame
        5,2,noir,pion
        6,1,blanc,dame


        Args:
            nom_fichier (str): Le nom du fichier où sauvegarder.

        """
        ma_partie =open(nom_fichier, 'w')
        ma_partie.write(self.couleur_joueur_courant)
        ma_partie.write("\n")
        ma_partie.write(str(self.doit_prendre))
        ma_partie.write("\n")
        ma_partie.write(str(self.position_source_forcee))
        ma_partie.write("\n")
        ma_partie.write(self.damier.convertir_en_chaine())
        ma_partie.close()

    def charger(self, nom_fichier):
        """Charge une partie à partir d'un fichier. Le fichier a le même format que la méthode de sauvegarde.

        ATTENTION: N'oubliez pas de bien convertir les chaînes de caractères lues!

        Args:
            nom_fichier (str): Le nom du fichier à lire.

        """
        charge = open(nom_fichier, 'r')

        ma_partie = charge.read()

        charge.close()

        print("Vous venez de charger votre partie")

        return (ma_partie)




if __name__ == "__main__":
    # Point d'entrée du programme. On initialise une nouvelle partie, et on appelle la méthode jouer().
    partie = Partie()
    damier = Damier()
    print(damier)

    # Si on veut sauvegarder une partie.
    partie.sauvegarder("ma_partie.txt")

    # Si on veut charger un fichier.
    #partie.charger("exemple_partie.txt")

    # Note: Nous ne vous demandons pas d'inclure les fonctionnalités de sauvegarde et de chargement dans le menu de jeu.
    # Par contre, assurez-vous que vos méthodes fonctionnent en les testant! Ces méthodes seront réutilisées dans le
    # TP4.


    print("Bienvenue dans le jeu de dames virtuel!")
    partie.jouer()
    gagnant = partie.jouer()
    print("------------------------------------------------------")
    print("Partie terminée! Le joueur gagnant est le joueur", gagnant)

    #gagnant = partie.jouer()
    #partie.demander_positions_deplacement()
    #print("------------------------------------------------------")
    #print("Partie terminée! Le joueur gagnant est le joueur", gagnant)
    #partie.position_source_valide(Position(2,6))>>>>>>> Stashed changes
